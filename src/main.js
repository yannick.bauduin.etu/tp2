import data from "./data.js";
import Component from "./components/Component.js";
import Img from "./components/Img.js";

const title = new Component("h1", null, "La Carte");
document.querySelector(".pageTitle").innerHTML = title.render();
console.log("Generated : " + title.render());

/*
const img = new Component("img", {
  name: "src",
  value:
    "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300",
});
document.querySelector(".pageContent").innerHTML = img.render();
console.log("Generated : " + img.render());
*/

const img2 = new Img(
  "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300"
);

document.querySelector(".pageContent").innerHTML = img2.render();
console.log("Generated : " + img2.render());
