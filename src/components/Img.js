import Component from "./Component.js";

class Img extends Component {
  constructor(attribute) {
    super("img", { name: "src", value: attribute });
  }
}

export default Img;
