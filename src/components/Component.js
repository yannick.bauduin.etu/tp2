class Component {
  tagname;
  children;
  attribute;
  constructor(tagname, attribute, children) {
    this.tagname = tagname;
    this.attribute = attribute;
    this.children = children;
  }

  renderAttribute() {
    return `${this.attribute.name}="${this.attribute.value}"`;
  }

  render() {
    if (this.tagname == "img") {
      return `<${this.tagname} ${this.renderAttribute()}/>`;
    }
    return `<${this.tagname}>${this.children}</${this.tagname}>`;
  }
}
export default Component;
